﻿# generator-metalsmith-basic [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Yeoman generator for basic metalsmith static website (in typescript).

## Installation

First, install [Yeoman](http://yeoman.io) and generator-metalsmith-basic using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo typescript
npm install -g generator-metalsmith-basic
```

Then generate your new project:

```bash
yo metalsmith-basic
```


## Build

You can build your website using `make`:

```bash
make build
```

The static website output is created in the `output` folder by default.
Generally, you can customize your website by modifying the content files under the `input` folder and/or the layout files under the `layouts` folder.
You can also update the `src/index.ts` file for further customization.




## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Harry](https://gitlab.com/realharry)


[npm-image]: https://badge.fury.io/js/generator-metalsmith-basic.svg
[npm-url]: https://npmjs.org/package/generator-metalsmith-basic
[travis-image]: https://travis-ci.org/harrywye/generator-metalsmith-basic.svg?branch=master
[travis-url]: https://travis-ci.org/harrywye/generator-metalsmith-basic
[daviddm-image]: https://david-dm.org/harrywye/generator-metalsmith-basic.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/harrywye/generator-metalsmith-basic
